using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeWK01_classes;

namespace HomeWK01
{
    internal class Program
    {

        public static void methodZ(drobes drobe, int num)
        {
            Console.WriteLine($"Знамнатель {drobe.ReturnZnamenat()} изменяется на {num}");
        }
        public static void methodC(drobes drobe, int num)
        {
            Console.WriteLine($"Числитель {drobe.ReturnChislit()} изменяется на {num}");
        }

        static void Method(drobes deop,  int val)
        {
            
        }

        static void Main(string[] args)
        {
            
            drobes Drobe1 = new drobes(5, 10); //0.5
            Drobe1.DrobeChange += Method;
            Console.WriteLine($"{Drobe1.ReturnChislit()}" + "/" + $"{Drobe1.ReturnZnamenat()}"
                + "=" + $"{Drobe1.ToDecimal()}");

            drobes Drobe2 = new drobes(5, 20); //0.25
            Console.WriteLine($"{Drobe2.ReturnChislit()}" + "/" + $"{Drobe2.ReturnZnamenat()}"
                + "=" + $"{Drobe2.ToDecimal()}");

            drobes Drobe3 = Drobe1 + Drobe2; //0.75
            Drobe3.ChangeZnamenat += methodZ;
            Drobe3.ChangeChislit += methodC;
            Drobe3.Znamenat = 100;
            Drobe3.Chislit = 500;


            Console.WriteLine($"{Drobe3.ReturnChislit()}" + "/" + $"{Drobe3.ReturnZnamenat()}" +
                "=" + $"{Drobe3.ToDecimal()}");

            Console.ReadLine();

            Drobe1 = new drobes(5, 8, 10); //5.8
           

            Console.WriteLine($"{Drobe1[1]}/{(Drobe1[0])}");

            Drobe2 = new drobes(7, 10); //0.7

            Drobe3 = Drobe1 + Drobe2; //6.5
            Console.WriteLine(Drobe3.ToDecimal());

            Console.ReadLine();

            drobes Drobe4 = new drobes(100); //100
            drobes FinalResult = (Drobe1 + Drobe2) / Drobe3 * Drobe4 - Drobe2;
            Console.WriteLine(FinalResult.ToDecimal()); //9.3

            Console.ReadLine();

        }
    }
}
